
# Start redis in background
redis-stable/src/redis-server &

# Start celery worker in background
python -m celery -A app.celery worker --loglevel=info &

# Serve app in foreground
python app.py