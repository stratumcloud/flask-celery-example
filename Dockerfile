FROM python:3.8-slim-buster

# Add system dependencies
RUN apt update && apt upgrade -y
RUN apt install -y curl build-essential

# Copy everything into the app
COPY . .

# # Email environment vars
# Must have a SMTP app password to work :)
# ENV MAIL_USERNAME=
# ENV MAIL_PASSWORD=

# Install redis
RUN ./install-redis.sh

# Install requirements to venv
RUN pip install -r requirements.txt

# Ensure we have permissions to run the main script
RUN chmod +x ./run.sh

# Bind port from host machine
EXPOSE 5000

# Start the 3 services up
CMD ./run.sh